/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import org.zouhir.tracker.models.Item;
import org.zouhir.tracker.models.User;
import org.zouhir.tracker.utils.BCrypt;
import org.zouhir.tracker.utils.ConnectionFactory;
import org.zouhir.tracker.utils.SessionUtils;
import utils.DbHelper;

/**
 *
 * @author zouhirchahoud
 */
public class ItemDAO {
    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());
    
    
    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private List<Item> usersList = null;

    public ItemDAO() {
    }
    
    
    public static void addItem(Item item) {
        String name = item.getName();
        String supplier = item.getSupplier();
        String shippingFrom = item.getShippingFrom();
        Date pdate = item.getPurchasedDate();
        Date ddate = item.getDeliveryDate();
        Date alarm = item.getAlarm();
        int userId = item.getUserId();
        String url = item.getUrl();
        String comments = item.getComments();
        
        
        
        String query = "INSERT INTO \"ITEM\"" 
                + "(ITEM_NAME, ITEM_SUPPLIER, ITEM_URL, ITEM_PDATE, ITEM_DDATE, ITEM_ALARM, ITEM_COMMENTS, ITEM_ARRIVED, ITEM_ARRIVED_LATE, ITEM_FROM, ITEM_UID) VALUES" 
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try{
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, supplier);
            preparedStatement.setString(3, url);
            preparedStatement.setDate(4, new java.sql.Date(pdate.getTime()));
            preparedStatement.setDate(5, new java.sql.Date(ddate.getTime()));
            preparedStatement.setDate(6, new java.sql.Date(alarm.getTime()));
            preparedStatement.setString(7, comments);
            preparedStatement.setBoolean(8, false);
            preparedStatement.setBoolean(9, false);
            preparedStatement.setString(10, shippingFrom);
            preparedStatement.setInt(11, userId);
            
            // execute insert SQL stetement
            int affectedRows = preparedStatement.executeUpdate();
            
            if (affectedRows == 0) {
                throw new SQLException("Creating item failed, no rows affected.");
            }
            
        } catch (SQLException e){
            LOG.warning(e.getMessage());
        } finally {
            DbHelper.close(preparedStatement);
            DbHelper.close(connection);
        }
        
    }
    
    
    public List<Item> getUserItem(int uid, boolean activeOnly) throws SQLException{
        String query;
        if(activeOnly == true){
            query = "SELECT * FROM \"ITEM\" WHERE ITEM_UID="+uid+" and ITEM_ARRIVED='false'";
        } else {
            query = "SELECT * FROM \"ITEM\" WHERE ITEM_UID="+uid;
        }
        
        ResultSet rs = null;
        List<Item> list = new LinkedList();
        try {
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                Item item = new Item();
                item.setName(rs.getString("ITEM_NAME"));
                item.setSupplier(rs.getString("ITEM_SUPPLIER"));
                item.setUrl(rs.getString("ITEM_URL"));
                item.setUserId(rs.getInt("ITEM_UID"));
                item.setPurchasedDate(rs.getDate("ITEM_PDATE"));
                item.setDeliveryDate(rs.getDate("ITEM_DDATE"));
                item.setAlarm(rs.getDate("ITEM_ALARM"));
                item.setShippingFrom(rs.getString("ITEM_FROM"));
                item.setComments(rs.getString("ITEM_COMMENTS"));
                item.setHasArrived(rs.getBoolean("ITEM_ARRIVED"));
                item.setId(rs.getInt("ITEM_ID"));
                
                
                Date today = new Date();
                    

                if(rs.getDate("ITEM_DDATE").compareTo(today) < 0){
                    item.setOverdue(true);
                } else {
                    item.setOverdue(false);
                }
                 
                
                list.add(item);
            }
        } finally {
            DbHelper.close(rs);
            DbHelper.close(preparedStatement);
            DbHelper.close(connection);
        }
        
        if(list.size() < 1) {
            return null;
        } else {
            return list;
        }
    }
    
    public void updateArrival(int id){
        LOG.severe(String.valueOf(id));
        String query = "UPDATE \"ITEM\" SET ITEM_ARRIVED='TRUE' WHERE ITEM_ID="+id;
        try{
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            
            
            // execute insert SQL stetement
            int affectedRows = preparedStatement.executeUpdate();
            
            if (affectedRows == 0) {
                throw new SQLException("updating item failed, no rows affected.");
            }
            
        } catch (SQLException e){
            LOG.warning(e.getMessage());
        } finally {
            DbHelper.close(preparedStatement);
            DbHelper.close(connection);
        }
    }
    
}
