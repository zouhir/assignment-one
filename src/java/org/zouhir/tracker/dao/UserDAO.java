package org.zouhir.tracker.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import org.zouhir.tracker.models.User;
import org.zouhir.tracker.utils.BCrypt;
import org.zouhir.tracker.utils.ConnectionFactory;
import utils.DbHelper;

/**
 *
 * @author zouhirchahoud
 */
public class UserDAO {

    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());
    
    
    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private List<User> usersList = null;

    public UserDAO() {
    }
    
    public static List<User> getUserByEmail(String email) throws SQLException {
        String query = "SELECT * FROM \"USER\" WHERE USR_EMAIL ='"+email.toLowerCase()+"'";
        ResultSet rs = null;
        List<User> list = new LinkedList();
        try {
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                User user = new User();
                user.setId(rs.getInt("USR_ID"));
                user.setEmail(rs.getString("USR_EMAIL"));
                user.setName(rs.getString("USR_NAME"));
                user.setPassword(rs.getString("USR_PASSWORD"));
                list.add(user);
            }
        } finally {
            DbHelper.close(rs);
            DbHelper.close(preparedStatement);
            DbHelper.close(connection);
        }
        
        if(list.size() < 1) {
            return null;
        } else {
            return list;
        }
    }
    
    public static void addUser(User user) {
        String name = user.getName();
        String email = user.getEmail().toLowerCase();
        String password = user.getPassword();
        String hashedPassword;
        hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(12));
        
        String query = "INSERT INTO \"USER\"" 
                + "(USR_NAME, USR_EMAIL, USR_PASSWORD, USR_IS_ACTIVE, USR_CREATED_AT, USR_UPDATE_DATE) VALUES" 
                + "(?, ?, ?, ?, ?, ?)";
        
        try{
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, hashedPassword);
            preparedStatement.setBoolean(4, true);
            preparedStatement.setTimestamp(5, getCurrentTimeStamp());
            preparedStatement.setTimestamp(6, getCurrentTimeStamp());
            
            // execute insert SQL stetement
            int affectedRows = preparedStatement.executeUpdate();
            
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()){
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            
        } catch (SQLException e){
            LOG.warning(e.getMessage());
        } finally {
            DbHelper.close(preparedStatement);
            DbHelper.close(connection);
        }
    }
    
    public static boolean validate(String email, String password) throws SQLException {
       
        List<User> ul = getUserByEmail(email);
        
        if(ul == null){
            return false;
        } else {
            User user = ul.get(0);
            boolean valid = BCrypt.checkpw(password, user.getPassword());
            return valid;
        }
    }
    
    private User mapResults(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("USR_ID"));
        user.setName(rs.getString("USR_NAME"));
        user.setEmail(rs.getString("USR_EMAIL"));
        user.setIsActive(rs.getBoolean("USR_IS_ACTIVE"));
        user.setPassword(rs.getString("USR_PASSWORD"));
        user.setUpdatedAt(rs.getDate("USR_UPDATE_DATE"));
        user.setCreatedAt(rs.getDate("USR_CREATED_AT"));
        return user;
    }
    
    // Get current timestamp to insert
    private static java.sql.Timestamp getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}
}
