/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.controllers;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import javax.servlet.http.HttpSession;
import org.zouhir.tracker.dao.UserDAO;
import org.zouhir.tracker.models.User;
import org.zouhir.tracker.utils.SessionUtils;

/**
 * User login controller bean
 * @author zouhirchahoud
 */
@Named
@SessionScoped
public class LoginController implements Serializable{
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String validate() throws SQLException {
            boolean valid;
            valid = UserDAO.validate(email, password);
            if (valid) {
                HttpSession session = SessionUtils.getSession();
                List<User> ul = UserDAO.getUserByEmail(email);
                User user = ul.get(0);
                session.setAttribute("name", user.getName());
                session.setAttribute("email", user.getEmail());
                session.setAttribute("id", user.getId());
                return "home?faces-redirect=true";
            } else {
                FacesContext.getCurrentInstance().addMessage(
                    null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Incorrect Username and Passowrd",
                                    "Please enter correct username and Password"));
                    return "login";
            }
    }

    //logout event, invalidate session
    public String logout() {
            HttpSession session = SessionUtils.getSession();
            session.invalidate();
            return "login";
    }
}
