package org.zouhir.tracker.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import java.sql.SQLException;
import javax.faces.application.FacesMessage;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.zouhir.tracker.dao.UserDAO;
import org.zouhir.tracker.models.User;

/**
 *
 * @author zouhirchahoud
 */
@Named
@SessionScoped
public class RegisterController implements Serializable{
    
    private String name;
    private String email;
    private String password;
    private String address;
    private String longitude;
    private String latitude;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    
   
   public String submit() throws SQLException{
        System.out.println("RegisterController:: received valid email: " + email);
        System.out.println("RegisterController:: name: " + name);
        // Set the message here
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Email is OK", "ok");  
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        
        UserDAO dao = new UserDAO();
        dao.addUser(user);
        
        return "home?faces-redirect=true";
   }
    
}
