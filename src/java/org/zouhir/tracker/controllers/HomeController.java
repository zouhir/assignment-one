/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.controllers;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.zouhir.tracker.dao.ItemDAO;
import org.zouhir.tracker.models.Item;
import org.zouhir.tracker.utils.SessionUtils;

/**
 * 
 * @author zouhirchahoud
 */
@Named
@SessionScoped
public class HomeController implements Serializable{

    private static final Logger LOG = Logger.getLogger(HomeController.class.getName());
    
    private List<Item> itemList;

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
    
    /**
     * function runs on page first load, and every refresh
     */
    public void onload(){
        List<Item> li = null;
        ItemDAO dao = new ItemDAO();
        try {
            li = dao.getUserItem(Integer.parseInt(SessionUtils.getUserId()), true);
            if(li != null){
                setItemList(li);
            } else {
                setItemList(null);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Updates package arrival from home page
     * @param id 
     */
    public void updateArrival(int id){
        ItemDAO dao = new ItemDAO();
        dao.updateArrival(id);
        onload();
    }
    
}
