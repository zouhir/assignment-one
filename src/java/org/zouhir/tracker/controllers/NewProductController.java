/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.controllers;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.zouhir.tracker.dao.ItemDAO;
import org.zouhir.tracker.models.Item;
import org.zouhir.tracker.utils.SessionUtils;

/**
 *
 * @author zouhirchahoud
 */
@Named
@SessionScoped
public class NewProductController implements Serializable{

    private static final Logger LOG = Logger.getLogger(NewProductController.class.getName());
    
    HttpSession session = SessionUtils.getSession();
    
    private String uid = SessionUtils.getUserId();
    private String uname = SessionUtils.getUserName();
    
    private String name;
    private String supplier;
    private String url;
    private String pdate;
    private String ddate;
    private String shippingFrom;
    private String comments;
    private String alarm;
    

    public String getUid() {
        return uid;
    }

    public String getUname() {
        return uname;
    }
    
    public String getName() {
        return name;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getUrl() {
        return url;
    }

    public String getPdate() {
        return pdate;
    }

    public String getDdate() {
        return ddate;
    }

    public String getShippingFrom() {
        return shippingFrom;
    }

    public String getComments() {
        return comments;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPdate(String pdate) {
        this.pdate = pdate;
    }

    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public void setShippingFrom(String shippingFrom) {
        this.shippingFrom = shippingFrom;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    
    
    
    public String submit(){
        Item item = new Item();
        
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM, yyyy", Locale.ENGLISH);
        LocalDate pldate = LocalDate.parse(pdate, formatter);
        Date pdatef = Date.from(pldate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        LocalDate dldate = LocalDate.parse(ddate, formatter);
        Date ddatef = Date.from(dldate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        LocalDate alarmDate = dldate.minusDays(Integer.parseInt("1"));
        Date alarmDatef = Date.from(alarmDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        
        
        item.setName(name);
        item.setSupplier(supplier);
        item.setUrl(url);
        item.setUserId(Integer.parseInt(SessionUtils.getUserId()));
        item.setPurchasedDate(pdatef);
        item.setDeliveryDate(ddatef);
        item.setAlarm(alarmDatef);
        item.setShippingFrom(shippingFrom);
        item.setComments(comments);
        
        ItemDAO dao = new ItemDAO();
        dao.addItem(item);
        
        return "home?faces-redirect=true";
        
    }
    
}
