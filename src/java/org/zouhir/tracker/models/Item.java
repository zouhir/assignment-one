/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.models;

import java.util.Date;

/**
 * Class Item represents every item object has been ordered online and getting shipped to a user
 * @author zouhirchahoud
 */
public class Item {
    private int id;
    private String name;
    private String supplier;
    private String url;
    private Date purchasedDate;
    private Date deliveryDate;
    private String shippingFrom;
    private boolean hasArrived;
    private String comments;
    private Date alarm;
    private int userId;
    private boolean overdue;

    public Item() {
    }

    public Item(int id, String name, String supplier, String url, Date purchasedDate, Date deliveryDate, String shippingFrom, boolean hasArrived, String comments, Date alarm, int userId, boolean overdue) {
        this.id = id;
        this.name = name;
        this.supplier = supplier;
        this.url = url;
        this.purchasedDate = purchasedDate;
        this.deliveryDate = deliveryDate;
        this.shippingFrom = shippingFrom;
        this.hasArrived = hasArrived;
        this.comments = comments;
        this.alarm = alarm;
        this.userId = userId;
        this.overdue = overdue;
    }

    

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getUrl() {
        return url;
    }

    public Date getPurchasedDate() {
        return purchasedDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public String getShippingFrom() {
        return shippingFrom;
    }

    public boolean isHasArrived() {
        return hasArrived;
    }

    public String getComments() {
        return comments;
    }

    public Date getAlarm() {
        return alarm;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public boolean isOverdue() {
        return overdue;
    }
    
    

    public void setName(String name) {
        this.name = name;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPurchasedDate(Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setShippingFrom(String shippingFrom) {
        this.shippingFrom = shippingFrom;
    }

    public void setHasArrived(boolean hasArrived) {
        this.hasArrived = hasArrived;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setAlarm(Date alarm) {
        this.alarm = alarm;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setOverdue(boolean overdue) {
        this.overdue = overdue;
    }
    
    
}
