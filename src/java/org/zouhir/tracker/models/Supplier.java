/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.models;

/**
 * Supplier class holds properties for the vendor or supplier who's shipping an item
 * @author zouhirchahoud
 */
public class Supplier {
    private int id;
    private String name;
    private String url;
    private String email;

    public Supplier() {
    }

    public Supplier(int id, String name, String url, String email) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getEmail() {
        return email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
