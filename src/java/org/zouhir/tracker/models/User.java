/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zouhir.tracker.models;

import java.util.Date;
import javax.annotation.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * User class has login details as well as profile for every user
 * @author zouhirchahoud
 */
public class User {
    private int id;
    private String name;
    private String email;
    private String password;
    private String gravatarUrl;
    private Date createdAt;
    private Date updatedAt;
    private boolean isActive;
    private Address defaultAddress;

    public User() {
    }

    public User(int id, String name, String email, String password, String gravatarUrl, Date createdAt, Date updatedAt, boolean isActive, Address defaultAddress) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.gravatarUrl = gravatarUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.isActive = isActive;
        this.defaultAddress = defaultAddress;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getGravatarUrl() {
        return gravatarUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public Address getDefaultAddress() {
        return defaultAddress;
    }
    
    

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGravatarUrl(String gravatarUrl) {
        this.gravatarUrl = gravatarUrl;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setDefaultAddress(Address defaultAddress) {
        this.defaultAddress = defaultAddress;
    }
}
